import sys
import os
import numpy as np
from pprint import pprint
from copy import deepcopy
from hbin import HBin
from tests.test_hbin import *


def test_basics():
    print('test_basics()')
    p = HBin.build(deepcopy(parted_year))
    print(p.to_string())
    pprint(p.to_dict())
    print(p, len(p), p[0], p[len(p) - 1])
    print(len(list(p)), list(p))


def test_mul():
    print('test_mul()')
    p = HBin.build(deepcopy(parted_year))
    q = HBin.build(deepcopy(parted_year))
    print(p)
    print(sum(p), sum(p * q), sum(p * 1), sum(p * 2), sum(p * 3))
    print(p * 3)


def test_npify():
    p = HBin.build(deepcopy(parted_year))
    a = np.array(p)
    print(len(a), len(p), a[13], p[13])


def test_get_values():
    p = HBin.build(deepcopy(parted_year))
    print(list(p.values()))
    print([o.value for o in p.values()])
    for v in p.values():
        v.value += 1
    print([o.value for o in p.values()])


def test_binning():
    p = HBin.build(deepcopy(parted_year))
    p.clear()
    p.binList(range(0, len(p)))
    for v in p.values():
        print('%s %s %s %s %s %s ' % (v.label, v.value, v.min(), v.max(), v.avg(), v._cnt))
    p.clear()
    for v in p.values():
        print('%s %s %s %s %s %s ' % (v.label, v.value, v.min(), v.max(), v.avg(), v._cnt))


def test_build_sum():
    ''' Build a HBin from a flat list by hierarchical binning. '''
    p = HBin.build(deepcopy(parted_year))
    print(list(p))
    p.clear(0)
    print(list(p))
    p.binList(range(0, len(p)))
    print(list(p))
    print(p.to_string())


def test_mins_maxs_sums_avgs():
    p = HBin.build(deepcopy(parted_year))
    p = p.binList(range(0, len(p)))
    print(p.to_string())
    print(p.mins().to_string())
    print(p.maxs().to_string())
    print(p.sums().to_string())
    print(p.avgs().to_string())


def test_arrays():
    p = HBin.build(deepcopy(parted_year))
    d = np.random.random((len(p), 3))
    p = p.binList(d)
    for i in p:
        print(i)
    print('---')
    for i in p.values():
        print(i)
    print('---')
    print('min=%s\nmax=%s\nsum=%s\navg=%s\n' % (p.min(), p.max(), p.sum(), p.avg()))
    print('min=%s\nmax=%s\nsum=%s\navg=%s\n' % (np.min(d, axis=0), np.max(d, axis=0), np.sum(d, axis=0), np.average(d, axis=0)))
    print('---')
    print(p.to_string())
    print('---')
    pprint(p.to_dict())


test_basics()
test_arrays()
test_mul()
test_npify()
test_binning()
test_get_values()
test_build_sum()
test_mins_maxs_sums_avgs()
