import json
import numpy as np
from pprint import pprint
from hbin import HBin
from tests.test_hbin import *


class ObjectStrJSONEncoder(json.JSONEncoder):

    '''
    The default encoder will not attempt to stringify object. Override to do so.
    '''

    def default(self, o):
        '''
        Called when JSONEncoder has already failed.
        If this fails call JSONEncoder again to let it generate TypeError.
        '''
        value = None
        try:
            iterable = iter(o)
            value = list(iterable)
        except TypeError:
            value = str(o)
        if value is None:
            json.JSONEncoder.default(self, o)
        return value


def test_json_dumps_scalars():
    p = HBin.build(parted_year)
    print(json.dumps(p.to_dict(), cls=ObjectStrJSONEncoder))


def test_json_dumps_arrays():
    p = HBin.build(parted_year)
    d = np.random.random((len(p), 3))
    p = p.binList(d)
    print(json.dumps(p.to_dict(), cls=ObjectStrJSONEncoder))


test_json_dumps_scalars()
print('---')
test_json_dumps_arrays()
