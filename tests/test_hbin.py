import unittest
from unittest import TestCase
from copy import deepcopy
from hbin import *
from tariffs.interval_usage_structures import *
import numpy as np
import timeit


# Simple single valued structure.
day = {
    'label': 'day',
    'width': 12,
    'value': 0.20
}
week = {
    'label': 'week',
    'width': 7,
    'value': day
}
year = {
    'label': 'year',
    'width': 52,
    'value': week
}

# Complex tariff structure. Days have length 4, and years 3, for brevity.
# 2*2+1+1 = 6
weekday_day = {
    'label': 'day',
    'width': 4,
    'children': [
        {'label': 'off', 'width': 1, 'value': 1.},
        {'label': 'on', 'width': 2, 'value': 2.},
        {'label': 'off', 'width': 1, 'value': 1.},
    ]
}
# 2*1+0.5+0.5 = 3
weekend_day = {
    'label': 'day',
    'width': 4,
    'children': [
        {'label': 'off', 'width': 1, 'value': 0.5},
        {'label': 'on', 'width': 2, 'value': 1},
        {'label': 'off', 'width': 1, 'value': 0.5},
    ]
}
# 5*6=30
weekday = {
    'label': 'weekday',
    'width': 5,
    'value': weekday_day
}
# 2*3=6
weekend = {
    'label': 'weekend',
    'width': 2,
    'value': weekend_day
}
# 36
parted_week = {
    'label': 'week',
    'width': 7,
    'children': [weekday, weekend]
}
# 3*36
parted_year = {
    'label': 'year',
    'width': 3,
    'value': parted_week
}


class TestHBin(TestCase):

    ''' Test HBin '''

    def test_basics(self):
        p = HBin.build(parted_year)
        self.assertEqual(len(p), 84)
        self.assertEqual(len(p), len(list(p)))

    def test_getitem(self):
        p = HBin.build(parted_year)
        self.assertEqual(p[0], 1)
        self.assertEqual(p[len(p) - 1], 0.5)

    def test_mul(self):
        p = HBin.build(parted_year)
        q = p * p
        self.assertTrue((q == np.array(list(p))**2).all())
        self.assertEqual(sum(p * 3), sum(p) * 3)

    def test_stats(self):
        p = HBin.build(parted_year)
        self.assertEqual(p.sum(), 3 * 36)
        self.assertEqual(p.min(), 0.5)
        self.assertEqual(p.max(), 2)
        self.assertEqual(p.avg(), p.sum() / len(p))
        p.clear()
        self.assertEqual(p.sum(), 0)
        self.assertEqual(p.min(), 0)
        self.assertEqual(p.max(), 0)

    def test_binning(self):
        p = HBin.build(parted_year)
        p = p.binList(np.ones(len(p)))
        self.assertEqual(p.sum(), len(p))
        p = p.binList(np.zeros(len(p)))
        self.assertEqual(p.sum(), 0)
        q = p.binList(range(0, len(p)))
        self.assertEqual(q.min(), 0)
        self.assertEqual(q.max(), len(q) - 1)

    def test_ndarray_values(self):
        p = HBin.build(deepcopy(parted_year))
        d = [np.random.randint(0, 10, 10) for i in range(0, len(p))]
        p = p.binList(d)
        self.assertEqual(len(list(p)), len(d))
        self.assertEqual(len(list(p)[0]), len(d[0]))
        self.assertEqual(len(p.min()), 10)
        self.assertEqual(len(p.max()), 10)
        self.assertTrue((p.min() - np.min(d, axis=0) < 1e-10).all())
        self.assertTrue((p.max() - np.max(d, axis=0) < 1e-10).all())
        self.assertTrue((p.sum() - np.sum(d, axis=0) < 1e-10).all())
        self.assertTrue((p.avg() - np.average(d, axis=0) < 1e-10).all())

    def test_array_values(self):
        ''' Normal lists don't work currently. Even if they did there is a bug with HBin.(min|max)()
        for lists - not ndarray though.
        '''
        p = HBin.build(deepcopy(parted_year))
        d = [list(np.random.randint(0, 10, 10)) for i in range(0, len(p))]
        with self.assertRaises(Exception):
            p = p.binList(d)

    def test_time(self):
        ''' Run timing tests to ensure time remain semi reasonable, given scope of HBin. '''
        t = timeit.Timer(lambda: self.timeit_mul()).timeit(number=1)
        print('\n' + str(t))
        self.assertTrue(t < 0.25)

        for s in [flat_daily_structure, standard_usage_structure, flat_weekly_structure, flat_monthly_structure]:
            t = timeit.Timer(lambda: self.timeit_hbin(s)).timeit(number=1)
            print('\n' + str(t))
            self.assertTrue(t < 0.8)

    def timeit_mul(self):
        ''' Time __mul__() '''
        p = HBin.build(standard_usage_structure)
        np.arange(0, len(p))
        p = p * p

    def timeit_hbin(self, s):
        ''' Time binList() '''
        p = HBin.build(s)
        p = p.binList(range(0, len(p)))
