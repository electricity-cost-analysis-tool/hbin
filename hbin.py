import numpy as np
import numbers
import json
import builtins
from copy import deepcopy


class HBin:

    ''' This class implements a hierarchical partition of a fixed `width` 1 dimensional integer domain.
    Each partition has a width and is either sub divided into sub partitions or has a single value. If
    the partition has a value, the class acts as if the value was repeated `width` times across the
    given partition. The value can be primitive number, or another HBin (hence 'hierarchical').

    To construct a HBin use the build() factory method not the constructor.

    The class also supports filling out a (copy of a) HBin via ~binning. I.e. you take a bunch of
    numbers stuff them into a HBin, and binning stats min, max, avg are calculated for each "bin" that
    number ends up in. Note that HBin is intended to be immutable - `binList()` returns a copy.

    The HBin class represents all nodes types, and plays different roles depepending on it's settings
    - which is a bit confusing ... The roles are:

        - Node with children.
        - Node with a value that is itself a HBin.
        - Node with a value that is a single number (values have to be numbers).

    If `children` is set, the HBin is partitioned into len(children) partitions which behave as described
    above. The invariant `parent.width = sum([len(c.width) for c in parent.children])` is maintained.
    '''
    width = None                    # Integer width of partition
    children = None             # The partition of this HBin. [HBin]|None.
    label = ''                        # String label for convenience.
    value = None                    # Value for this partition. numbers.Number|HBin|None
    _min = None                     # Leaf bin minimum statistic.
    _max = None                     # Leaf bin maximum statistic.
    _cnt = 0                            # Leaf bin count statistic. This is ony used as a working var to calc avg.
    _len = None                     # Stash length of this HBin, since structure is statistic.

    def __init__(self, width, value=None, children=None, label=None):
        ''' A not very useful constructor. Should be considered private. @see build() factory method. '''
        (self.width, self.value, self.children, self.label) = (width, value, children, label)

    def __str__(self):
        return str((self.label, self.width, self.value))

    def __iter__(self):
        ''' Iterate over domain. '''
        if self.is_leaf():
            for i in range(0, self.width):
                if isinstance(self.value, HBin):
                    for j in self.value:
                        yield j
                else:
                    yield self.value
        else:
            for c in self.children:
                for i in c:
                    yield i

    def __len__(self):
        ''' Number of values in domain (recursive). Stash calculated len coz it's expected to be static. '''
        if self._len is None:
            if self.is_leaf():
                if isinstance(self.value, HBin):
                    self._len = len(self.value) * self.width
                else:
                    self._len = self.width
            else:
                self._len = sum([len(v) for v in self.children])
        return self._len

    def __getitem__(self, k):
        ''' Get the k-th virtual item. '''
        return self.getValueNode(k).value

    def __mul__(self, o):
        ''' Multiply self by `o` returning flat array? '''
        return np.array(self) * o

    def binList(self, o):
        ''' This operation is logically like taking each element of `o` and sorting it into the correct
        ~bin based on it's index.
        '''
        if len(self) != len(o):
            raise ValueError('Wrong length %d != %d' % (len(self), len(o)))
        new = self.clone().clear(0)
        k = 0
        while k < len(o):
            node = new.getValueNode(k)  # Will return same node for next node.width indexes.
            for j in range(k, k + node.width):
                node._addValue(o[j])
            k = k + node.width
        for v in new.values():
            v.value = v.value / v._cnt
        return new

    def mins(self):
        ''' Swap the value of all value nodes with the min bin stat of that value node and return copy. '''
        new = self.clone()
        for node in new.values():
            node.clear(node.min())
        return new

    def maxs(self):
        ''' Swap the value of all value nodes with the max bin stat of that value node and return copy. '''
        new = self.clone()
        for node in new.values():
            node.clear(node.max())
        return new

    def sums(self):
        ''' Swap the value of all value nodes with the sum bin stat of that value node and return copy. '''
        new = self.clone()
        for node in new.values():
            node.clear(node.sum())
        return new

    def avgs(self):
        ''' Swap the value of all value nodes with the avg bin stat of that value node and return copy. '''
        new = self.clone()
        for node in new.values():
            node.clear(node.value)
        return new

    def getValueNode(self, k):
        ''' Returns the leaf node HBin corresponding to index `k`. '''
        if type(k) != int:
            raise KeyError()
        if k >= len(self):
            raise IndexError()
        if self.is_leaf():
            if isinstance(self.value, HBin):
                return self.value.getValueNode(k % len(self.value))
            else:
                return self
        else:
            for c in self.children:
                if len(c) > k:
                    return c.getValueNode(k)
                else:
                    k -= len(c)

    def values(self):
        ''' Iterate over leaf value nodes. Returns HBin itself if is value node. '''
        if self.is_leaf():
                if isinstance(self.value, HBin):
                    for j in self.value.values():
                        yield j
                else:
                    yield self
        else:
            for c in self.children:
                for i in c.values():
                    yield i

    def has_children(self):
        return self.children is not None

    def is_leaf(self):
        return self.value is not None

    def clear(self, v=0):
        ''' Recursively set of values to `value` and clear state. '''
        if self.is_leaf():
            if isinstance(self.value, HBin):
                self.value.clear(v)
            else:
                self._clearValue(v)
        else:
            for c in self.children:
                c.clear(v)
        return self

    def _addValue(self, v):
        ''' Add value to the value of this leaf HBin. This is only applicable to leaves. '''
        self.value += v
        self._min = min(self._min, v) if self._cnt else v
        self._max = max(self._max, v) if self._cnt else v
        self._cnt += 1

    def _clearValue(self, v=0):
        ''' Set node value and binning stats to value. After clear it as if the HBin has a single
        homogeneous value of `value` (min = max = value). Setting the interval variable _cnt is set to
        zero indicates the HBin is not a resultant of binList(), and is used by binList(). This method
        is only applicable to leaves.
        '''
        self.value = self._min = self._max = v
        self._cnt = 0

    def avg(self):
        ''' Get avg, which is just the value of a primitive value node. '''
        if self.is_leaf():
            if isinstance(self.value, HBin):
                return self.value.avg()
            else:
                return self.value
        else:
            return sum([c.avg() * c.width for c in self.children]) / self.width

    def min(self):
        ''' Get min primitive value under this node.'''
        if self.is_leaf():
            if isinstance(self.value, HBin):
                return self.value.min()
            else:
                return self._min
        else:
            return min([c.min() for c in self.children])

    def max(self):
        ''' Get max primitive value under this node.'''
        if self.is_leaf():
            if isinstance(self.value, HBin):
                return self.value.max()
            else:
                return self._max
        else:
            return max([c.max() for c in self.children])

    def sum(self):
        ''' Get max primitive value under this node.'''
        if self.is_leaf():
            if isinstance(self.value, HBin):
                return self.value.sum() * self.width
            else:
                return self.value * self.width
        else:
            return sum([c.sum() for c in self.children])

    def clone(self):
        return deepcopy(self)

    def to_string(self, depth=1):
        ''' Get string rep of entire hierarchy under self. '''
        delim = '; ' if isinstance(self.avg(), numbers.Number) else '\n' + (depth * '\t')  # Hack for pretty array values.
        node_formatter = '{indent}label={label}; width={width}; len={len}{delim}\
sum={sum}{delim}\
min={min}{delim}\
max={max}{delim}\
value={value}\n'
        _str = node_formatter.format(
            label=self.label,
            width=self.width,
            len=len(self),
            sum=self.sum(),
            min=self.min(),
            max=self.max(),
            value=self.avg(),
            indent=(depth - 1) * '\t',
            delim=delim
        )
        if self.is_leaf():
            if isinstance(self.value, HBin):
                _str += self.value.to_string(depth + 1)
        else:
            for c in self.children:
                _str += c.to_string(depth + 1)
        return _str

    def to_dict(self):
        _dict = {
            'label': self.label,
            'width': self.width,
            'len': len(self),
            'sum': self.sum(),
            'min': self.min(),
            'max': self.max(),
            'avg': self.avg()
        }
        if self.is_leaf():
            if isinstance(self.value, HBin):
                _dict['value'] = self.value.to_dict()
        else:
            _dict['children'] = []
            for c in self.children:
                _dict['children'] += [c.to_dict()]
        return _dict

    @classmethod
    def build(cls, obj):
        return cls._build(json.loads(json.dumps(obj)))

    @classmethod
    def _build(cls, obj):
        ''' The static method hydrates a HBin from an object with the correct format. All the
        constructor logic is here not in __init__. The values in obj, can be numbers or ndarrays.
        @todo check widths actually match up.
        '''
        o = HBin(**obj)
        if o.has_children():
            for k, c in enumerate(o.children):
                o.children[k] = cls._build(o.children[k])
            if o.value is not None:
                raise ValueError()
        elif o.value is not None:
            if type(o.value) == dict:
                o.value = cls._build(o.value)
            elif isinstance(o.value, numbers.Number) or instance(o.value, np.ndarray):
                o._clearValue(float(o.value))
            else:
                raise ValueError()
        else:
            raise ValueError()
        return o


def min(*o):
    ''' min that works with np.arrays too. '''
    try:
        return builtins.min(*o)  # This doesn't fail on array of list but does on array of ndarray ...
    except ValueError as e:
        if len(o) == 1:
            return np.min(*o, axis=0)
        else:
            return np.min(o, axis=0)


def max(*o):
    ''' max that works with np.arrays too. '''
    try:
        return builtins.max(*o)  # This doesn't fail on array of list but does on array of ndarray ...
    except ValueError as e:
        if len(o) == 1:
            return np.max(*o, axis=0)
        else:
            return np.max(o, axis=0)


def sum(o):
    ''' sum that works with np.arrays too. '''
    try:
        return builtins.sum(o)
    except TypeError as e:
        return np.sum(o, axis=0)
